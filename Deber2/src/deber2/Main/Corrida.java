package deber2.Main;

public class Corrida {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cuadrado c1 = new Cuadrado(45);
		c1.calcularArea();
		c1.calcularPerimetro();
		System.out.println(" \n Rectangulo: \n ");
		Rectangulo r2 = new Rectangulo(10,15);
		r2.calcularArea();
		r2.calcularPerimetro();
		System.out.println(r2.toString());
		//enums y sobrecarga
		Reuniones r = new Reuniones(DiasDeLaSemana.LUNES);
		r.diasDeFiesta();
		r.diasDeFiesta(5);
		r.disDeFiesta("semana sin fiesta , muy ocupada!");
		
	}

}
